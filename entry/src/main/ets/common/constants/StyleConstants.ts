/**
 * @Project: tds-ui
 * @Name: StyleConstants
 * @Create: 2024/01/05 09:36
 * @Author: Gorit
 * @IDLE: DevEco Studio
 * @Description:
**/

/**
 * 控件圆角大小场景
 * https://developer.harmonyos.com/cn/docs/design/des-guides/rounded-corner-0000001156978413#section2568122510283
 */
export class Radius {
  static readonly small = "4vp";
  static readonly progress = "2vp";
  static readonly subPage = "4vp";
  static readonly radio = "4vp";
  static readonly tapEffect = "8vp";
  static readonly icon = "8vp";
  static readonly switch = "10vp";

  static readonly gridItem = "12vp";
  static readonly normal = "8vp"; // 通用小圆角
  static readonly medium = "12vp";  // 通用中圆角
  static readonly banner = "12vp";
  static readonly large = "16vp"; // 通用大圆角
  static readonly card = "16vp";
  static readonly popup = "16vp";
  static readonly share = "16vp";
  static readonly menu = "20vp";
  static readonly largest = "24vp"; // 通用大圆角

  static readonly normalBtn = "20vp";
  static readonly smallBtn = "14vp";
}

/**
 * Color
 * 支持 HEX 类型 与 透明度进行拼接
 */
export class Color {
  static readonly Opacity_100 = "FF"
  static readonly Opacity_99 = "FC"
  static readonly Opacity_98 = "FA"
  static readonly Opacity_97 = "F7"
  static readonly Opacity_96 = "F5"
  static readonly Opacity_95 = "F2"
  static readonly Opacity_94 = "F0"
  static readonly Opacity_93 = "ED"
  static readonly Opacity_92 = "EB"
  static readonly Opacity_91 = "E8"
  static readonly Opacity_90 = "E6"
  static readonly Opacity_89 = "E3"
  static readonly Opacity_88 = "E0"
  static readonly Opacity_87 = "DE"
  static readonly Opacity_86 = "DB"
  static readonly Opacity_85 = "D9"
  static readonly Opacity_84 = "D6"
  static readonly Opacity_83 = "D4"
  static readonly Opacity_82 = "D1"
  static readonly Opacity_81 = "CF"
  static readonly Opacity_80 = "CC"
  static readonly Opacity_79 = "C9"
  static readonly Opacity_78 = "C7"
  static readonly Opacity_77 = "C4"
  static readonly Opacity_76 = "C2"
  static readonly Opacity_75 = "BF"
  static readonly Opacity_74 = "BD"
  static readonly Opacity_73 = "BA"
  static readonly Opacity_72 = "B8"
  static readonly Opacity_71 = "B5"
  static readonly Opacity_70 = "B3"
  static readonly Opacity_69 = "B0"
  static readonly Opacity_68 = "AD"
  static readonly Opacity_67 = "AB"
  static readonly Opacity_66 = "A8"
  static readonly Opacity_65 = "A6"
  static readonly Opacity_64 = "A3"
  static readonly Opacity_63 = "A1"
  static readonly Opacity_62 = "9E"
  static readonly Opacity_61 = "9C"
  static readonly Opacity_60 = "99"
  static readonly Opacity_59 = "96"
  static readonly Opacity_58 = "94"
  static readonly Opacity_57 = "91"
  static readonly Opacity_56 = "8F"
  static readonly Opacity_55 = "8C"
  static readonly Opacity_54 = "8A"
  static readonly Opacity_53 = "87"
  static readonly Opacity_52 = "85"
  static readonly Opacity_51 = "82"
  static readonly Opacity_50 = "80"
  static readonly Opacity_49 = "7D"
  static readonly Opacity_48 = "7A"
  static readonly Opacity_47 = "78"
  static readonly Opacity_46 = "75"
  static readonly Opacity_45 = "73"
  static readonly Opacity_44 = "70"
  static readonly Opacity_43 = "6E"
  static readonly Opacity_42 = "6B"
  static readonly Opacity_41 = "69"
  static readonly Opacity_40 = "66"
  static readonly Opacity_39 = "63"
  static readonly Opacity_38 = "61"
  static readonly Opacity_37 = "5E"
  static readonly Opacity_36 = "5C"
  static readonly Opacity_35 = "59"
  static readonly Opacity_34 = "57"
  static readonly Opacity_33 = "54"
  static readonly Opacity_32 = "52"
  static readonly Opacity_31 = "4F"
  static readonly Opacity_30 = "4D"
  static readonly Opacity_29 = "4A"
  static readonly Opacity_28 = "47"
  static readonly Opacity_27 = "45"
  static readonly Opacity_26 = "42"
  static readonly Opacity_25 = "40"
  static readonly Opacity_24 = "3D"
  static readonly Opacity_23 = "3B"
  static readonly Opacity_22 = "38"
  static readonly Opacity_21 = "36"
  static readonly Opacity_20 = "33"
  static readonly Opacity_19 = "30"
  static readonly Opacity_18 = "2E"
  static readonly Opacity_17 = "2B"
  static readonly Opacity_16 = "29"
  static readonly Opacity_15 = "26"
  static readonly Opacity_14 = "24"
  static readonly Opacity_13 = "21"
  static readonly Opacity_12 = "1F"
  static readonly Opacity_11 = "1C"
  static readonly Opacity_10 = "1A"
  static readonly Opacity_9 = "17"
  static readonly Opacity_8 = "14"
  static readonly Opacity_7 = "12"
  static readonly Opacity_6 = "0F"
  static readonly Opacity_5 = "0D"
  static readonly Opacity_4 = "0A"
  static readonly Opacity_3 = "08"
  static readonly Opacity_2 = "05"
  static readonly Opacity_1 = "03"
  static readonly Opacity_0 = "00"


}

/**
 * 鸿蒙官网：字号规范
 * https://developer.harmonyos.com/cn/docs/design/des-guides/font-0000001110498550#section1928399162216
 */
export class Font {

  static readonly Headline1 = {
    fontSize: 96,
    fontWeight: FontWeight.Lighter
  };

  static readonly Headline2 = {
    fontSize: 72,
    fontWeight: FontWeight.Lighter
  };

  static readonly Headline3 = {
    fontSize: 60,
    FontWeight: FontWeight.Lighter
  };


  static readonly Headline4 = {
    fontSize: 48,
    FontWeight: FontWeight.Regular
  };

  static readonly Headline5 = {
    fontSize: 38,
    FontWeight: FontWeight.Regular
  };

  // 大标题
  static readonly Headline6 = {
    fontSize: 30,
    FontWeight: FontWeight.Medium
  };

  // 二级标题，强调文本
  static readonly Headline7 = {
    fontSize: 24,
    FontWeight: FontWeight.Medium
  };

  // 页签标题
  static readonly Headline8 = {
    fontSize: 20,
    FontWeight: FontWeight.Medium
  };

  // 分组大标题
  static readonly Subtitle1 = {
    fontSize: 18,
    FontWeight: FontWeight.Medium
  }

  // 分组标题
  static readonly Subtitle2 = {
    fontSize: 16,
    FontWeight: FontWeight.Medium
  }

  // 分组小标题
  static readonly Subtitle3 = {
    fontSize: 14,
    FontWeight: FontWeight.Medium
  }

  // 列表正文文本/段落文本
  static readonly Body1 = {
    fontSize: 16,
    FontWeight: FontWeight.Regular
  }

  // 列表辅助文本/段落文本
  static readonly Body2 = {
    fontSize: 14,
    FontWeight: FontWeight.Regular
  }

  // 列表辅助文本/图文说明
  static readonly Body3 = {
    fontSize: 12,
    FontWeight: FontWeight.Regular
  }

  // 大按钮文本
  static readonly Button1 = {
    fontSize: 16,
    FontWeight: FontWeight.Medium
  }

  // 小按钮文本
  static readonly Button2 = {
    fontSize: 14,
    FontWeight: FontWeight.Medium
  }

  // 隐私说明文本
  static readonly Caption = {
    fontSize: 10,
    FontWeight: FontWeight.Regular
  }

  // 大标题辅助说明文本
  static readonly Overline = {
    fontSize: 14,
    FontWeight: FontWeight.Regular
  }
}